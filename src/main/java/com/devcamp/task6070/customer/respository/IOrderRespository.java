package com.devcamp.task6070.customer.respository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.devcamp.task6070.customer.model.COrder;
public interface IOrderRespository extends JpaRepository<COrder,Long>{
    
}
