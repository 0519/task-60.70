package com.devcamp.task6070.customer.respository;

import com.devcamp.task6070.customer.model.CCustomer;

import org.springframework.data.jpa.repository.JpaRepository;

public interface ICustomerRespository extends JpaRepository<CCustomer,Long>{
    CCustomer findByCustomerId(Long customerId);
}
